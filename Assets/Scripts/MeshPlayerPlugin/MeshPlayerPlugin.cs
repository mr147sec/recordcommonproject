using UnityEngine;
using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.IO;
using System.Threading;

public class MeshPlayerPlugin : MonoBehaviour
{
    // source and play
    public bool isMulti;
    public string mSourceUrl = "";
    public double mSourceDurationSec = 0, mSourceFPS = 0;
    public double mFirstPts = -101, mLastPts = -101;
    public int mSourceNbFrames = 0;

    private string mCurrentSourceUrl;
    private bool mIsOpened, mIsPlaying, mForwardOnce, mIsQuit;
    private bool mIsCubeVisible;
    private bool mInited = false;
    // cube的mesh实例
    private Mesh mCubeMesh;
    private List<MeshFilter> meshFilters;
    private List<Mesh> mCubeMeshs;
    private Texture2D mTexture, mCubeTexture;
    private int mTextureWidth, mTextureHeight;
    private int mApiKey;

    // background thread
    Thread mThread;

    //debug
    private static Boolean mIsShowDebugInfo = false;
    private static List<string> mDebugMessage = new List<string>();

    #region MeshData
    // mesh data one for edit, one for display
    // double buffer scheme
    struct MeshData
    {
        public double pts_sec;
        public Vector3[] vertices;
        public Vector3[] normals;
        public Vector2[] uv;
        public int[] triangles;
        public Color32[] colors;
        public bool ready;
        public int buffer_size;

        public void clear()
        {
            ready = false;
            Array.Clear(colors, 0, colors.Length);
            colors = null;

            buffer_size = 0;
            vertices = null;
            normals = null;
            uv = null;
            triangles = null;
        }

        public void allocColors(int width, int height)
        {
            colors = new Color32[width * height];
        }

        public void allocBuffers(int count)
        {
            if (buffer_size < count || buffer_size > count * 2)
            {
                buffer_size = ((int)(count * 1.1) / 3 + 1) * 3;
                vertices = new Vector3[buffer_size];
                normals = new Vector3[buffer_size];
                uv = new Vector2[buffer_size];
                triangles = new Int32[buffer_size];
            }
        }

        public Vector3[] GetVertices(int count)
        {
            allocBuffers(count);
            return vertices;
        }

        public Vector3[] GetNormals(int count)
        {
            allocBuffers(count);
            return normals;
        }

        public Vector2[] GetUv(int count)
        {
            allocBuffers(count);
            return uv;
        }

        public Int32[] GetTriangles(int count)
        {
            allocBuffers(count);
            return triangles;
        }
    }
    private MeshData[] mMeshDataBuffer = new MeshData[2];
    private int mEditIdx = 0, mDisplayIdx = 1;
    public int mFrameIdx = -1;
    private object mMeshDataLockd = new object();
    #endregion

    #region PluginAPI
#if UNITY_IPHONE && !UNITY_EDITOR
    private const string DllName = "__Internal";
#elif UNITY_ANDROID && !UNITY_EDITOR
    private const string DllName = "MeshPlayerPlugin";
#else
    private const string DllName = "MeshPlayerPlugin";
#endif

    [DllImport(DllName)]
    public static extern void UnityPluginUnload();

    [DllImport(DllName)]
    public static extern void SetDebugFunction(IntPtr func);

    [DllImport(DllName)]
    private static extern int CreateApiInstance();

    [DllImport(DllName)]
    private static extern bool OpenMeshStream(int api, string src);

    [DllImport(DllName)]
    private static extern void GetMeshStreamInfo(int api, ref double duration_sec, ref double fps, ref int nb_frames);

    [DllImport(DllName)]
    private static extern void PlayReader(int api);

    [DllImport(DllName)]
    private static extern void PauseReader(int api);

    [DllImport(DllName)]
    public static extern void CloseReader(int api);

    [DllImport(DllName)]
    private static extern void GetResolution(int api, ref int width, ref int height);

    [DllImport(DllName)]
    private static extern bool BeginReadFrame(int api, ref double pts_sec);

    [DllImport(DllName)]
    private static extern void EndReadFrame(int api);

    [DllImport(DllName)]
    private static extern int GetVerticesCount(int api);

    [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern void
        SetMeshVertices(int api, int count, IntPtr vertices, IntPtr normals, IntPtr uvs, IntPtr triangles);

    [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern void SetMeshTextures(int api, int width, int height, int channels, IntPtr colors);
    #endregion

    #region Player Control
    public void StartPlaying()
    {
        // open source
        if (!mIsOpened)
        {
            if (!OpenCurrentSource())
            {
                Debug.Log("Open Source Failed");
                return;
            }
        }

        PlayReader(mApiKey);
        mIsPlaying = true;
        mCurrentSourceUrl = mSourceUrl;
    }

    public void PausePlaying()
    {
        PauseReader(mApiKey);
        mIsPlaying = false;
    }

    public void StopPlaying()
    {
        CloseReader(mApiKey);
        mIsPlaying = false;
        mIsOpened = false;
    }

    public void OpenCurrentUrl(string url)
    {
        mSourceUrl = url;
        OpenCurrentSource();
    }

    public bool OpenCurrentSource()
    {
        mIsOpened = OpenMeshStream(mApiKey, mSourceUrl);

        if (mIsOpened)
        {
            GetMeshStreamInfo(mApiKey, ref mSourceDurationSec, ref mSourceFPS, ref mSourceNbFrames);
            Debug.Log("[MeshPlayerPlugin] Duration = " + mSourceDurationSec);
            Debug.Log("[MeshPlayerPlugin] FPS = " + mSourceFPS);
            Debug.Log("[MeshPlayerPlugin] Number of Frames = " + mSourceNbFrames);
            mTimer = 0;
            if (mSourceUrl.StartsWith("rtmp") || mSourceUrl.StartsWith("rtsp") || mSourceUrl.StartsWith("http") || mSourceUrl.StartsWith("tcp"))
                mIgnoreTimer = true;
            else
                mIgnoreTimer = false;

            //做判断，避免相同url的视频重复new texture和color32数组
            if (mCurrentSourceUrl != mSourceUrl)
            {
                //先销毁之前的
                if (mTexture != null)
                {
                    GetComponent<Renderer>().material.mainTexture = null;
                    Destroy(mTexture);
                    mTexture = null;
                    mCubeTexture = null;
                    mMeshDataBuffer[0].clear();
                    mMeshDataBuffer[1].clear();

                    //为了避免显示白模，先将MeshRender隐藏
                    GetComponent<MeshRenderer>().enabled = false;
                    mIsCubeVisible = false;
                }

                int width = 0, height = 0;
                GetResolution(mApiKey, ref width, ref height);
                if (width > 0 && height > 0)
                {
                    Debug.Log("texture " + width + ", " + height);
                    mTexture = new Texture2D(width, height, TextureFormat.ARGB32, false);
                    mTextureWidth = width;
                    mTextureHeight = height;
                    mMeshDataBuffer[0].allocColors(width, height);
                    mMeshDataBuffer[1].allocColors(width, height);
                }
            }

            // read one frame
            mForwardOnce = true;
            StartPlaying();
        }
        else
        {
            Debug.Log("open failed");
        }

        return mIsOpened;
    }

    private unsafe void SetMeshVerticesByApi(int count)
    {
        ref MeshData meshData = ref mMeshDataBuffer[mEditIdx];

        // pin memory
        fixed (Vector3* pVertices = meshData.GetVertices(count), pNormals = meshData.GetNormals(count))
        {
            fixed (Vector2* pUvs = meshData.GetUv(count))
            {
                fixed (Int32* pTriangles = meshData.GetTriangles(count))
                {
                    SetMeshVertices(mApiKey, meshData.buffer_size, (IntPtr)pVertices, (IntPtr)pNormals, (IntPtr)pUvs, (IntPtr)pTriangles);
                }
            }
        }

        //mCubeMesh.Clear();
        //mCubeMesh.vertices = vertices;
        //mCubeMesh.normals = normals;
        //mCubeMesh.uv = uv;
        //mCubeMesh.triangles = triangles;
        //mCubeMesh.RecalculateNormals();
    }

    private unsafe void SetMeshTextureByApi()
    {
        ref MeshData meshData = ref mMeshDataBuffer[mEditIdx];

        fixed (Color32* pColors = meshData.colors)
        {
            SetMeshTextures(mApiKey, mTextureWidth, mTextureHeight, 4, (IntPtr)pColors);
        }

        //mTexture.SetPixels32(mColors);
        //mTexture.Apply();

        ////GetComponent开销很大，这里需要做条件约束避免每帧都跑GetComponent方法
        //if (mCubeTexture != mTexture)
        //{
        //    GetComponent<Renderer>().material.mainTexture = mTexture;
        //    mCubeTexture = mTexture;
        //}
    }

    public static string frameIndex;

    public string GetFrame()
    {
        return frameIndex;
    }

    // callback for debug
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DebugDelegate(string str);

    [AOT.MonoPInvokeCallback(typeof(DebugDelegate))]
    static void CallbackDebug(string str)
    {
#if UNITY_STANDALONE
        mDebugMessage.Add(str);

        if (str.Contains("#"))
        {
            var s = str.Split('#');
            if (s[1] != null)
            {
                frameIndex = s[1];
            }

        }

        if (mDebugMessage.Count > 40)
            mDebugMessage.RemoveAt(0);
        Debug.Log(str);
#else
        Debug.Log(str);
#endif
    }

    public void SwitchShowDebugInfo()
    {
        mIsShowDebugInfo = !mIsShowDebugInfo;
        Debug.Log("SwitchShowDebugInfo() = " + mIsShowDebugInfo);
    }
    #endregion

    #region Background Thread
    void StartThread()
    {
        mThread = new Thread(new ThreadStart(updateMeshData));
        mThread.IsBackground = true;//防止后台现成。相反需要后台线程就设为false
        mThread.Start();
    }

    void updateMeshData()
    {
        while (!mIsQuit && mMeshDataLockd != null)
        {
            if (mMeshDataBuffer[mEditIdx].ready && mMeshDataBuffer[mDisplayIdx].ready)
                continue;

            double pts_sec = 0;
            if (!mMeshDataBuffer[mEditIdx].ready && !mMeshDataBuffer[mDisplayIdx].ready && (mIsPlaying || mForwardOnce) && BeginReadFrame(mApiKey, ref pts_sec))
            {
                //   Debug.Log("[thread] read to edit");

                int count = GetVerticesCount(mApiKey);
                SetMeshVerticesByApi(count);
                SetMeshTextureByApi();
                EndReadFrame(mApiKey);
                mMeshDataBuffer[mEditIdx].pts_sec = pts_sec;
                mMeshDataBuffer[mEditIdx].ready = true;
                Debug.Log("[thread] pts = " + pts_sec);

                if (mForwardOnce)
                {
                    mForwardOnce = false;
                }
            }

            if (mMeshDataBuffer[mEditIdx].ready && !mMeshDataBuffer[mDisplayIdx].ready)
            {
                //  Debug.Log("[thread] update to display");

                lock (mMeshDataLockd)//防止其他线程访问当前线程使用的数据
                {
                    mEditIdx = (mEditIdx + 1) % 2;
                    mDisplayIdx = (mEditIdx + 1) % 2;
                    mFrameIdx++;
                }
            }
        }

        Debug.Log("thread stopped");
        //Thread.Sleep(1000);
    }
    #endregion

    #region Unity Routines

    public void SetMulti(List<MeshFilter> meshs)
    {
        isMulti = true;
        mCubeMeshs = new List<Mesh>();
        meshFilters = meshs;
        for (int i = 0; i < meshs.Count; i++)
        {
            mCubeMeshs.Add(meshs[i].GetComponent<MeshFilter>().mesh);
        }
    }

    void Awake()
    {
        //StartThread();
    }

    // pipeline
    public void InitPlugin()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
		RegisterPlugin();
#endif

#if UNITY_EDITOR
        // set debug function

#endif
        DebugDelegate callback_delegate = new DebugDelegate(CallbackDebug);
        IntPtr intptr_delegate = Marshal.GetFunctionPointerForDelegate(callback_delegate);
        SetDebugFunction(intptr_delegate);

        //获取mesh实例
        GetComponent<MeshRenderer>().enabled = false;

        if (!isMulti)
        {
            mCubeMesh = GetComponent<MeshFilter>().mesh;
        }


        //创建实例
        mApiKey = CreateApiInstance();

        //第一帧
        mIsOpened = false;
        mIsPlaying = false;
        mForwardOnce = false;

        //StartPlaying();

        ////GetComponent开销很大，避免每帧都跑GetComponent方法
        //GetComponent<Renderer>().material.mainTexture = mTexture;

        mIsQuit = false;
        StartThread();
        mInited = true;
    }

    private double mTimer = 0.0f;
    public bool mIgnoreTimer = false;
    public bool mIsTimerExternal = false;

    public void SetTimer(float t)
    {
        mTimer = t;
        mIsTimerExternal = true;
    }

    private bool CheckTimer(double pts_sec)
    {
        return mIgnoreTimer || mTimer >= pts_sec;
    }

    private bool updateFirstLastPts(double pts_sec)
    {
        // first frame
        if (mFirstPts < -100)
        {
            mFirstPts = pts_sec;
            mLastPts = mFirstPts;
            return false;
        }

        // not first frame
        else
        {
            // new frame: update last pts
            if (pts_sec > mLastPts)
            {
                mLastPts = pts_sec;
                return false;
            }

            // first frame again
            else if (pts_sec == mFirstPts)
            {
                return true;
            }
        }

        return false;
    }

    public void SetMaterials(Material mat)
    {
        if (isMulti)
        {
            for (int i = 0; i < meshFilters.Count; i++)
            {
                //根据材质贴图变量名称修改
                meshFilters[i].GetComponent<Renderer>().material = mat;
                // meshFilters[i].GetComponent<Renderer>().material.SetTexture("_UnlitColorMap", mTexture);
                mCubeTexture = mTexture;
            }
        }
        else
        {
            GetComponent<Renderer>().material = mat;
            GetComponent<Renderer>().material.mainTexture = mTexture;
            mCubeTexture = mTexture;
        }
    }

    private void updateNormalIfNeeded(ref Mesh mesh)
    {
        if (mesh.vertices.Length < 3)
            return;

        bool needs_update = false;
        if (mesh.vertices.Length != mesh.normals.Length)
        {
            needs_update = true;
        }
        else if (mesh.normals[0][0] == 0 && mesh.normals[0][1] == 0 && mesh.normals[0][2] == 0)
        {
            needs_update = true;
        }

        if (needs_update)
        {
            mesh.RecalculateNormals();
        }
        
    }

    void Update()
    {
        if (!mInited)
        {
            return;
        }

        if (!mIsTimerExternal && mIsPlaying)
            mTimer += Time.deltaTime;

        // update pts
        bool isLooped = updateFirstLastPts(mMeshDataBuffer[mDisplayIdx].pts_sec);
        if (isLooped)
        {
            Debug.Log("mTimer " + mTimer + " -> " + mFirstPts);
            mTimer = mFirstPts;
        }

        if (mMeshDataBuffer[mDisplayIdx].ready && CheckTimer(mMeshDataBuffer[mDisplayIdx].pts_sec))
        {
            lock (mMeshDataLockd)
            {
                ref MeshData meshData = ref mMeshDataBuffer[mDisplayIdx];

                if (isMulti)
                {
                    for (int i = 0; i < mCubeMeshs.Count; i++)
                    {
                        var mesh = mCubeMeshs[i];
                        mesh.Clear();
                        mesh.vertices = meshData.vertices;
                        mesh.normals = meshData.normals;
                        mesh.uv = meshData.uv;
                        mesh.triangles = meshData.triangles;
                        updateNormalIfNeeded(ref mesh);
                    }
                }
                else
                {
                    mCubeMesh.Clear();
                    mCubeMesh.vertices = meshData.vertices;
                    mCubeMesh.normals = meshData.normals;
                    mCubeMesh.uv = meshData.uv;
                    mCubeMesh.triangles = meshData.triangles;
                    updateNormalIfNeeded(ref mCubeMesh);
                }

                mTexture.SetPixels32(meshData.colors);
                mTexture.Apply();


                meshData.ready = false;
            }

            if (isMulti)
            {
                for (int i = 0; i < meshFilters.Count; i++)
                {
                    //根据材质贴图变量名称修改
                    meshFilters[i].GetComponent<Renderer>().material.mainTexture = mTexture;
                    // meshFilters[i].GetComponent<Renderer>().material.SetTexture("_UnlitColorMap", mTexture);
                    mCubeTexture = mTexture;
                }
            }
            else
            {
                if (mCubeTexture != mTexture)
                {
                    //根据材质贴图变量名称修改
                    GetComponent<Renderer>().material.mainTexture = mTexture;
                    // GetComponent<Renderer>().material.SetTexture("_UnlitColorMap", mTexture);
                    mCubeTexture = mTexture;
                }
            }

            //GetComponent开销很大，这里需要做条件约束避免每帧都跑GetComponent方法


            if (mIsCubeVisible == false)
            {
                GetComponent<MeshRenderer>().enabled = true;
                mIsCubeVisible = true;
            }
        }
    }

    void OnGUI()
    {
        if (mIsShowDebugInfo)
            GUI.Label(new Rect(15, 45, 900, 900), String.Join("\n", mDebugMessage.ToArray()));
    }

    private void OnDisable()
    {
        Debug.Log("[MeshPlayerPlugin] OnDisable()");
        mIsQuit = true;
        mThread.Join();
        CloseReader(mApiKey);
    }

    private void OnDestroy()
    {
        Debug.Log("[MeshPlayerPlugin] OnDestroy()");
        UnityPluginUnload();
    }
    #endregion

}