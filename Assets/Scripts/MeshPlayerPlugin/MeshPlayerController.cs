﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MeshPlayerController : MonoBehaviour
{
    public bool isMulti;
    public List<MeshFilter> meshFilters;
    public int modelID;
    public int mSourceIdx = 0;
    public string sourceUrl;
    private MeshPlayerPlugin meshPlayerPlugin;
    public bool mIsPlaying;
    public List<Material> materials;
    public Text frame;
    private void Awake()
    {
        meshPlayerPlugin = gameObject.AddComponent<MeshPlayerPlugin>();
        //meshPlayerPlugin.mSourceUrl = GetUrlForStreamingAssets(sourceUrl);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        if (isMulti)
        {
            meshPlayerPlugin.SetMulti(meshFilters);
        }
        else
        {

        }
        meshPlayerPlugin.InitPlugin();

        //自动加载StreamingAssets下的内容
        OpenSource(GetUrlForStreamingAssets(sourceUrl));
    }

    public void OpenSource(string Url) {
        if (mIsPlaying)
        {
            meshPlayerPlugin.StopPlaying();
        }
        else {
        }
        mIsPlaying = true;
        meshPlayerPlugin.OpenCurrentUrl(Url);

    }

    // Update is called once per frame
    void Update()
    {
        if (mIsPlaying) {
           // frame.text = $"f:{meshPlayerPlugin.GetFrame()}";
        }
    }

    public void SetMaterial(int mat) {
        if (mat == 0) {
            meshPlayerPlugin.SetMaterials(materials[0]);
        }
        else if (mat == 1) {
            meshPlayerPlugin.SetMaterials(materials[1]);
        }
    }


    public float timer;
    public void FixedUpdate()
    {
        //timer += Time.deltaTime;
        //meshPlayerPlugin.SetTimer(timer);
    }

    public void PlayOrPause()
    {
        if (mIsPlaying)
        {
            PauseVideo();
        }
        else
        {
            PlayVideo();
        }
    }

    public void SetTimer(float time)
    {
        meshPlayerPlugin.SetTimer(time);
    }

    public void ReStart()
    {
        StopVideo();
        PlayVideo();
    }

    public void PlayVideo()
    {
        meshPlayerPlugin.StartPlaying();
        mIsPlaying = true;
    }

    public void PauseVideo()
    {
        meshPlayerPlugin.PausePlaying();
        mIsPlaying = false;
    }

    public void StopVideo()
    {
        meshPlayerPlugin.StopPlaying();
        mIsPlaying = false;
    }

    public void CheckIsEnd()
    {
        
    }

    private string GetUrlForStreamingAssets(string filename)
    {
        if (filename.StartsWith("rtmp"))
            return filename;

#if UNITY_ANDROID && !UNITY_EDITOR
        string url = Path.Combine(Application.streamingAssetsPath, filename);
        WWW wwwfile = new WWW(url);
        while (!wwwfile.isDone) { }
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, "mesh.mp4");
        File.WriteAllBytes(filepath, wwwfile.bytes);
        return filepath;
#else
        return Path.Combine(Application.streamingAssetsPath, filename);
#endif
    }
}
