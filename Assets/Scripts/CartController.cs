﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartController : MonoBehaviour
{
    public CinemachineDollyCart cinemachineDollyCart;
    public float pos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (cinemachineDollyCart.m_Position > pos)
        {
            cinemachineDollyCart.m_Speed = -cinemachineDollyCart.m_Speed;
        }
        else if(cinemachineDollyCart.m_Position ==0) {
            cinemachineDollyCart.m_Speed = -cinemachineDollyCart.m_Speed;
        }
    }
}
